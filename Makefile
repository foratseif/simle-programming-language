CC=gcc
FLAGS=-Wall -Isrc/engine
LIBS=-ldl
BIN=bin

all: dirs engine

### BIN DIRECTORY --
dirs: $(BIN)

$(BIN):
	mkdir -p $(BIN)


### ENGINE ---------

ENGINE=src/engine
ENGINE_INCLUDES=
engine: engine_tools engine_runtime engine_lang engine_res engine_tests

# - ENGINE TOOLS FILES --------------------------
ENGINE_TOOLS=$(ENGINE)/tools
ENGINE_TOOLS_CFILES  := $(wildcard $(ENGINE_TOOLS)/*.c)
ENGINE_TOOLS_OBJECTS := $(patsubst %.c,%.o, $(ENGINE_TOOLS_CFILES))

engine_tools: $(ENGINE_TOOLS_OBJECTS)

# loop though engine tools files
$(ENGINE_TOOLS_OBJECTS): $(ENGINE_TOOLS)/%.o: $(ENGINE_TOOLS)/%.c
	$(CC) $(FLAGS) -c $^ $(LIBS) -o $@

# add compiled engine tools to ENGINE_INCLUDES
ENGINE_INCLUDES+=$(ENGINE_TOOLS_OBJECTS)

# - ENGINE RUNTIME FILES --------------------------
ENGINE_RUNTIME=$(ENGINE)/runtime
ENGINE_RUNTIME_CFILES  := $(wildcard $(ENGINE_RUNTIME)/*.c)
ENGINE_RUNTIME_OBJECTS := $(patsubst %.c,%.o, $(ENGINE_RUNTIME_CFILES))

engine_runtime: $(ENGINE_RUNTIME_OBJECTS)

# loop though engine runtime files
$(ENGINE_RUNTIME_OBJECTS): $(ENGINE_RUNTIME)/%.o: $(ENGINE_RUNTIME)/%.c
	$(CC) $(FLAGS) -c $^ $(LIBS) -o $@

# add compiled engine runtime to ENGINE_INCLUDES
ENGINE_INCLUDES+=$(ENGINE_RUNTIME_OBJECTS)

# - ENGINE LANG FILES --------------------------
ENGINE_LANG=$(ENGINE)/lang
ENGINE_LANG_CFILES  := $(wildcard $(ENGINE_LANG)/*.c)
ENGINE_LANG_OBJECTS := $(patsubst %.c,%.o, $(ENGINE_LANG_CFILES))

engine_lang: $(ENGINE_LANG_OBJECTS)

# loop though engine lang files
$(ENGINE_LANG_OBJECTS): $(ENGINE_LANG)/%.o: $(ENGINE_LANG)/%.c
	$(CC) $(FLAGS) -c $^ $(LIBS) -o $@

# add compiled engine lang to ENGINE_INCLUDES
ENGINE_INCLUDES+=$(ENGINE_LANG_OBJECTS)

# - ENGINE RESOURCES ---------------------------
ENGINE_RES=$(ENGINE)/res
ENGINE_RES_CFILES  := $(shell find $(ENGINE_RES)/ -name "*.c")
#ENGINE_RES_CFILES  := $(wildcard $(ENGINE_RES)/*.c)
ENGINE_RES_OBJECTS := $(patsubst %.c,%.o, $(ENGINE_RES_CFILES))

engine_res: $(ENGINE_RES_OBJECTS)

# loop though engine resource files
$(ENGINE_RES_OBJECTS): $(ENGINE_RES)/%.o: $(ENGINE_RES)/%.c
	$(CC) $(FLAGS) -c $^ $(LIBS) -o $@

# add compiled engine resources to ENGINE_INCLUDES
ENGINE_INCLUDES+=$(ENGINE_RES_OBJECTS)

# - ENGINE TEST FILES ---------------------------
ENGINE_TEST_SRC=$(ENGINE)/tests
ENGINE_TEST_DST=$(BIN)
ENGINE_TEST_CFILES  := $(wildcard $(ENGINE_TEST_SRC)/*.c)
ENGINE_TEST_OBJECTS := $(patsubst $(ENGINE_TEST_SRC)/%.c, $(ENGINE_TEST_DST)/%.o, $(ENGINE_TEST_CFILES))

engine_tests: $(ENGINE_TEST_OBJECTS)

# loop though engine test files
$(ENGINE_TEST_OBJECTS): $(ENGINE_TEST_DST)/%.o: $(ENGINE_TEST_SRC)/%.c $(ENGINE_INCLUDES)
	$(CC) $(FLAGS) $^ $(LIBS) -o $@


clean_engine:
	rm -f $(ENGINE_TOOLS)/*.o
	rm -f $(ENGINE_LANG)/*.o
	rm -f $(ENGINE_RUNTIME)/*.o
	rm -f $(ENGINE_RES)/*.o
	rm -f $(ENGINE_RES)/**/*.o
	rm -f $(BIN)/*.o

### CLEAN --------
clean: clean_engine
