#ifndef _ENVIRONMENT_H_
#define _ENVIRONMENT_H_

#include <stdlib.h>
#include <stdio.h>

typedef struct simle_env_st simle_env;

// customs
#include <lang/simle_var.h>
#include <tools/list.h>

// environment structure definition
struct simle_env_st {
    simle_env* parent;
    list* vars;
};

simle_env* init_env(simle_env* parent);
void clear_env(simle_env* env);

void       env_set_var(simle_env* env, char* name, simle_obj* o);
simle_var* env_get_var(simle_env* env, char* name);

#endif // _ENVIRONMENT_H_
