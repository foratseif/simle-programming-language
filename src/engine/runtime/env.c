#include "env.h"

// initialize env
simle_env* init_env(simle_env* parent){
    // allocate environment
    simle_env* env = (simle_env*) calloc(1, sizeof(simle_env));

    // init tracking variables
    env->vars = list_new();

    // set parent to NULL
    env->parent = parent;

    // return env
    return env;
}


// get var if in environment
simle_var* env_get_var(simle_env* env, char* name){

    // loop through vars in environment and try to find matching var
    while(env != NULL){
        simle_var* v;
        foreach(v, env->vars){
            if(strcmp(name, v->name) == 0){
                return v;
            }
        }
        env = env->parent;
    }

    return NULL;
}

// add variable to environment
void env_var_add(simle_env* env, simle_var* v){
    list_add(env->vars, v);
}

// environment variable set
void env_set_var(simle_env* env, char* name, simle_obj* o){
    // loop through vars in environment and try to find matching var
    simle_var* v;
    foreach(v, env->vars){
        if(strcmp(name, v->name) == 0){
            break;
        }
    }

    if(v != NULL){
        simle_var_set(v, o);
    }else{
        v = simle_var_create(name);
        simle_var_set(v, o);
        env_var_add(env, v);
    }
}

// clear the environment struct before destroying
void clear_env(simle_env* env){
    // quit if pointer is empty
    if(env == NULL) return;

    // clear lists before clearing env pointer
    if(env->vars != NULL){
        simle_var* v;

        foreach_clear(v, env->vars){
            simle_var_free(v);
        }

        free(env->vars);
    }

    // free allocated environment
    free(env);
}

