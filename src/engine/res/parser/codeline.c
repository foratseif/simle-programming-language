#include "codeline.h"

void free_codeline(codeline* cl){

    // check if pointer has value
    if(cl == NULL)
        return;

    // free all words
    if(cl->th != NULL){
        free_thunk(cl->th);
    }

    // free codeline struct
    free(cl);
}
