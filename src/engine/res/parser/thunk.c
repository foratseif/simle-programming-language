#include "thunk.h"

/*
// creates thunk out of codeline
thunk* create_thunk(codeline* cl){
    return create_sub_thunk(cl, 0, cl->count);
}

thunk* create_sub_thunk(codeline* cl, int start, int end){
    thunk* th = (thunk*) malloc(sizeof(thunk) + (sizeof(char*) * (end-start)));

    th->count = end-start;

    int i, j = 0;
    for(i=start; i<end; i++){
        th->words[j] = (char*) calloc(sizeof(char), strlen(cl->words[i])+1);
        strcpy(th->words[j], cl->words[i]);
        j++;
    }

    return th;
}
*/

// allocate memory for a thunk with 'count' amount of words
thunk* malloc_thunk(int count){
    thunk* th = (thunk*) malloc(sizeof(thunk) + (sizeof(char*) * count));
    th->count = count;
    return th;
}

// free thunk
void free_thunk(thunk* th){

    int i;
    for(i=0; i<th->count; i++){
        free(th->words[i]);
    }

    free(th);
}

// create a sub thunk out of a thunk
thunk* sub_thunk(thunk* th, int start, int stop){
    int new_count = stop-start;

    thunk* sth = (thunk*) malloc(sizeof(thunk) + (sizeof(char*) * new_count));

    sth->count = new_count;

    int i;
    for(i=start; i<stop; i++){
        sth->words[i-start] = (char*) calloc(sizeof(char), strlen(th->words[i])+1);
        strcpy(sth->words[i-start], th->words[i]);
    }

    return sth;
}

// create a sub thunk out of a thunk
thunk* clone_thunk(thunk* th){
    return sub_thunk(th, 0, th->count);
}

char* thunk2str(thunk* th){
    char* ret = malloc(sizeof(char)*100);
    ret[0] = '\0';
    int x;
    for(x=0; x<th->count; x++){
        if(x) strcat(ret, " ");
        strcat(ret, th->words[x]);
    }
    return ret;
}
