#ifndef _CODE_LINE_H_
#define _CODE_LINE_H_

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <inttypes.h>

typedef struct codeline_st codeline;

#include <res/parser/thunk.h>

struct codeline_st {
    uint8_t indent;
    thunk*  th;
};

void free_codeline(codeline* cl);

#endif // _CODELINE_H_
