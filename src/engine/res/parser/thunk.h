#ifndef _THUNK_H_
#define _THUNK_H_

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

typedef struct thunk_st thunk;

// customs
#include <res/parser.h>
#include <res/interpreter/command.h>

struct thunk_st {
    uint8_t count;
    char*   words[];
};

thunk* create_thunk(codeline* cl);
thunk* create_sub_thunk(codeline* cl, int start, int end);
thunk* malloc_thunk(int count);
void free_thunk(thunk* th);

thunk* sub_thunk(thunk* th, int start, int stop);
thunk* clone_thunk(thunk* th);

char* thunk2str(thunk* th);

#endif // _THUNK_H_
