#ifndef _SIMLE_ERR_H_
#define _SIMLE_ERR_H_

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>

typedef enum {
    syntax_error_no_class_name = 1,
    syntax_error_no_ext_class_name,
    syntax_error_unexpected_word,
    syntax_error_unexpected_token,
    syntax_error_unexpected_indent,
    syntax_error_premature_line_break,
    syntax_error_empty_path,
    syntax_error_already_used_once_in_method_def,
    syntax_error_illegal_variable_name,
    syntax_error_wrong_first_var,
    syntax_error_class_variable_outside_class,
    syntax_error_method_already_defined,
    syntax_error_cant_interpret_line,
    syntax_error_incomplete_return_statement,
    syntax_error_var_name_empty,
    runtime_error,
    runtime_error_class_not_found,
    runtime_error_clib_late_set,
    runtime_error_parent_late_set,
    runtime_error_class_has_no_clib,
    runtime_error_var_not_found,
    runtime_error_method_not_found
} simle_err_type;

char* simle_err_get(void);
void  simle_err_set(simle_err_type err, ... );
void  simle_err_set_lineno(int lineno);

#ifndef SIMLE_ERR_MAXLEN
#define SIMLE_ERR_MAXLEN 501
#endif

#endif // _SIMLE_ERR_H_
