#include "memory.h"

// initialize memory
simle_mem* init_memory(){
    // allocate memory
    simle_mem* mem = (simle_mem*) calloc(1, sizeof(simle_mem));

    // init tracking classes
    mem->class_list = list_new();

    // init global environment
    mem->global_env = init_env(NULL);

    // return mem
    return mem;
}

// add class to memory
void memory_class_add(simle_mem* mem, simle_class* c){
    list_add(mem->class_list, c);
}

// get class if in memory
simle_class* memory_class_get(simle_mem* mem, char* name){

    // loop through classes in memory and try to find matching class
    simle_class* c;
    foreach(c, mem->class_list){
        if(strcmp(name, c->name) == 0){
            return c;
        }
    }

    return NULL;
}

// clear the memory struct before destroying
void clear_memory(simle_mem* mem){
    // quit if pointer is empty
    if(mem == NULL) return;

    // clear lists before clearing mem pointer
    if(mem->class_list != NULL){
        simle_class* c;

        foreach_clear(c, mem->class_list){
            simle_class_free(c);
        }

        free(mem->class_list);
    }

    // clear global environment
    if(mem->global_env != NULL){
        clear_env(mem->global_env);
    }

    // free allocated memory
    free(mem);
}

// set global var
void memory_set_global_var(simle_mem* m, char* name, simle_obj* o){
    env_set_var(m->global_env, name, o);
}

// get global var
simle_var* memory_get_global_var(simle_mem* m, char* name){
    return env_get_var(m->global_env, name);
}


void dump_vars(list* l, char* indent, char pre){
    simle_var* v;
    foreach(v, l){
        printf("%svar: '%c%s' = (%s)\n", indent, pre, v->name, ( v->obj == NULL ? "null" : "obj" ));
    }
}

// print out memory contents for debugging
void dump_memory(simle_mem* mem){
    printf("## MEMORY DUMP: ##\n");

    printf("#\n");

    printf("#   VARS: %d\n", mem->global_env->vars->size);
    dump_vars(mem->global_env->vars, "#   - ", '$');

    printf("#\n");

    printf("#   CLASSES: %d\n", mem->class_list->size);

    // loop through classes
    simle_class* c;
    foreach(c, mem->class_list){
        // print main class stuff
        printf("#   - class: %s (methods: %d | statics: %d) ", c->name, c->methods->size, c->s_methods->size);

        // check for bridge
        if(c->bridge != NULL)
            printf("(bridged) ");
        else
            printf("          ");

        // check for class parent
        if(c->parent != NULL)
            printf("(extends: %s) ", c->parent->name);

        printf("\n");

        printf("#      - vars: %d\n", c->vars->size);
        dump_vars(c->vars, "#          - ", '@');

        // prep to loop through methods
        simle_method* m;

        // loop through methods
        foreach(m, c->methods){

            // print method info
            printf("#      - method");
            printf("\n");

            // print caller position
            printf("#          - caller_pos: %d\n", m->caller_pos);

            // print definition
            printf("#          - definition: ");
            int i;
            for(i=0; i<m->seq_c; i++){
                if(i == m->caller_pos){
                    printf("[object] ");
                }else{
                    printf("%s ", m->seq_v[i]);
                }
            }
            printf("\n");

            // print params
            if(m->params != NULL){
                printf("#          - params: ");
                char* s;
                foreach(s, m->params){
                    printf("(%s) ", s);
                }
                printf("\n");
            }

            // print commands
            if(m->block != NULL){
                if(m->block->env != NULL){
                    printf("#          - vars: %d\n", m->block->env->vars->size);
                    dump_vars(m->block->env->vars, "#              - ", '$');
                }

                printf("#          - commands: %d\n", m->block->len);

                for(i=0; i<m->block->len; i++){
                    printf("#              - ");
                    debug_print_command_type(m->block->commands[i]->type);
                    printf("\n");
                }

            }
        }
        // loop through static methods
        foreach(m, c->s_methods){
            // print method info
            printf("#      - static method");
            printf("\n");

            // print caller position
            printf("#          - caller_pos: %d\n", m->caller_pos);

            // print definition
            printf("#          - definition: ");
            int i;
            for(i=0; i<m->seq_c; i++){
                if(i == m->caller_pos){
                    printf("[class] ");
                }else{
                    printf("%s ", m->seq_v[i]);
                }
            }
            printf("\n");

            // print params
            if(m->params != NULL){
                printf("#          - params: ");
                char* s;
                foreach(s, m->params){
                    printf("(%s) ", s);
                }
                printf("\n");
            }

            // print commands
            if(m->block != NULL){
                if(m->block->env != NULL){
                    printf("#          - vars: %d\n", m->block->env->vars->size);
                    dump_vars(m->block->env->vars, "#              - ", '$');
                }

                printf("#          - commands: %d\n", m->block->len);
                for(i=0; i<m->block->len; i++){
                    printf("#              - ");
                    debug_print_command_type(m->block->commands[i]->type);
                    printf("\n");
                }
            }
        }
    }
}
