#ifndef _MEMORY_H_
#define _MEMORY_H_

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

// define memory structure
typedef struct simle_mem_st simle_mem;

// customs
#include <tools/list.h>
#include <lang/simle_class.h>
#include <runtime/env.h>

// memory structure definition
struct simle_mem_st {
    list* class_list;
    simle_env* global_env;
};

// memory functions
simle_mem* init_memory();
void clear_memory(simle_mem* mem);
void dump_memory(simle_mem* mem);

// memory simle_class functions
void memory_class_add(simle_mem* mem, simle_class* c);
simle_class* memory_class_get(simle_mem* mem, char* name);

void memory_set_global_var(simle_mem* m, char* name, simle_obj* o);
simle_var* memory_get_global_var(simle_mem* m, char* name);

#endif // _MEMORY_H_

