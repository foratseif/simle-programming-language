#include "bridge.h"

// create bridge
bridge* bridge_create(char* path){

    // get library handler
    void* handler = dlopen(path, RTLD_LAZY);

    // check for error
    if(handler == NULL){
        // set error
        simle_err_set(runtime_error, dlerror());
        return NULL;
    }

    // create bridge struct
    bridge* b = (bridge*) malloc(sizeof(bridge));
    b->handler = handler;

    return b;
}

// get a symbol from library through bridge
void* bridge_get_func(bridge* b, char* name){

    // fetch symbol
    void* f = dlsym(b->handler, name);

    // check for error
    if(f == NULL){
        // set error
        simle_err_set(runtime_error, dlerror());
        return NULL;
    }

    // return symbol
    return f;
}

// call function with arguments
void bridge_call_func(void* f, void* args[]){
    void (*func)(void*[]);
    func = f;
    func(args);
}

// call function directly from bridge
int bridge_call(bridge* b, char* name, void* args[]){
    // get function
    void* func = bridge_get_func(b, name);

    // check for errors
    if(func == NULL){
        return 1;
    }

    // call function
    bridge_call_func(func, args);
    return 0;
}

// free bridge struct
void bridge_free(bridge* b){
    if(b == NULL) return;

    if(b->handler != NULL){
        dlclose(b->handler);
    }

    free(b);
}
