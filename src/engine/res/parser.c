#include "parser.h"

codeline* parse_line(char* line){

    // replace # with \0
    char* ptr = line;
    while(*ptr != '\0'){
        if(*ptr == '#'){
            *ptr = '\0';
            break;
        }
        ptr++;
    }

    uint8_t count = 0;
    uint8_t indent = 0;
    char* pointers[256];

    int i;
    for(i=0; i<256; i++){
        pointers[i] = 0;
    }
    i=0;

    // count indent
    while(*line == ' '){
        line++;
        indent++;
    }

    int string_mode = 0;

    // parse string
    for(;;){
        while(*line == ' '){
            line++;
        }

        if(*line == '\0') break;

        char* START_OF_WORD = line;
        count ++;

        int word_len = 0;

        /* check for special syntaxes */

        // enter or leave string mode
        if(*line == '"'){
            word_len = 1;
            string_mode = (string_mode) ? 0 : 1;
            line ++;
        }

        // check string_mode
        else if(string_mode){
            // get whole string
            while(*line != '\0' && *line != '"'){
                word_len++;
                line++;
            }
        }

        // variable
        else if(*line == '$' || *line == '@'){
            do{
                word_len++;
                line++;
            }while(isalpha(*line) || isdigit(*line) || *line == '_');
        }

        else if(isalpha(*line) || isdigit(*line) || *line == '_'){
            int started_with_int = isdigit(*line);
            do{
                word_len++;
                line++;
                if(!started_with_int && word_len == 1 && isdigit(*line)){
                    break;
                }
            }while(isalpha(*line) || isdigit(*line) || *line == '_');
        }else{
            word_len++;
            line++;
        }

        if(word_len){
            pointers[i] = (char*) calloc(sizeof(char), word_len+1);
            memcpy(pointers[i], START_OF_WORD, word_len);
            i++;
        }
    }

    codeline* ret = (codeline*) malloc(sizeof(codeline));
    ret->th = malloc_thunk(count);
    ret->indent = indent;

    // check if there are words
    if(count > 0){
        // move pointers over
        for(i=0; i<count; i++){
            ret->th->words[i] = pointers[i];
        }
    }

    return ret;
}

