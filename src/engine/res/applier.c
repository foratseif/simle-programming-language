#include "applier.h"

// todo-next:
//   * do methods and return
//   * check errors from env/obj get & set

simle_obj* apply_command_assignment(command* cmd, int* error);
simle_obj* apply_command_static_method(command* cmd, int* error);
simle_obj* apply_command_object_method(command* cmd, int* error);
simle_obj* apply_command_var_method(command* cmd, int* error);
simle_obj* apply_command_var_global(command* cmd, int* error);
simle_obj* apply_command_var_class(command* cmd, int* error);

struct context_st {
    simle_obj* o;
    simle_method* m;
};

simle_mem* mem = NULL;
struct context_st context;

void init_applier(simle_mem* m){
    mem = m;
    context.o = NULL;
    context.m = NULL;
}

void quit_applier(){
    mem = NULL;
}

simle_obj* apply_command(command* cmd, int* error){

    if(cmd->type == COMMAND_ASSIGNMENT){
        return apply_command_assignment(cmd, error);
    }

    else if(cmd->type == COMMAND_IF){
        debug_print_command_type(cmd->type); printf("\n");
    }

    else if(cmd->type == COMMAND_WHILE){
        debug_print_command_type(cmd->type); printf("\n");
    }

    else if(cmd->type == COMMAND_CLIB){
        debug_print_command_type(cmd->type); printf("\n");
    }

    else if(cmd->type == COMMAND_STATIC_METHOD){
        return apply_command_static_method(cmd, error);
    }

    else if(cmd->type == COMMAND_OBJECT_METHOD){
        return apply_command_object_method(cmd, error);
    }

    else if(cmd->type == COMMAND_YIELD){
        debug_print_command_type(cmd->type); printf("\n");
    }

    else if(cmd->type == COMMAND_VAR_METHOD){
        return apply_command_var_method(cmd, error);
    }

    else if(cmd->type == COMMAND_VAR_GLOBAL){
        return apply_command_var_global(cmd, error);
    }

    else if(cmd->type == COMMAND_VAR_CLASS){
        return apply_command_var_class(cmd, error);
    }

    return NULL;
}

simle_obj* apply_command_assignment(command* cmd, int* error){

    // ** assuming syntax is correct ** //

    // get value of assignment statement
    simle_obj* obj = apply_command(cmd->ptr[1], error);

    // check for error!
    if(*error){
        return NULL;
    }

    // get variable name
    char* var = cmd->ptr[0];

    // global variable
    if(context.m == NULL){
        // variable is global
        env_set_var(mem->global_env, var+1, obj);
    }

    // variable inside method
    else if(var[0] == '$'){
        env_set_var(context.m->block->env, var+1, obj);
    }

    // object variable (@var)
    else if(context.o != NULL){
        simle_obj_set_var(context.o, var+1, obj);
    }

    return obj;
}

simle_obj* apply_command_static_method(command* cmd, int* error){
    // create index for for-loops
    int i;

    // pointer to method
    simle_method* m = cmd->sm_method;

    // create env for block?
    simle_block* b = m->block;
    simle_block_init_env(b);

    // add params to env
    i = 0;
    char*  param;
    foreach(param, m->params){
        simle_obj* param_o = apply_command(cmd->ptr[i], error);

        if(*error){
            return NULL;
        }

        env_set_var(b->env, param, param_o);
        i++;
    }

    // backup context
    simle_method* cont_m = context.m;
    simle_obj*    cont_o = context.o;

    // set context
    context.m = cmd->sm_method;
    context.o = NULL;

    // run through method
    simle_obj* ret_obj = NULL;
    for(i=0; i<b->len; i++){
        command* c = b->commands[i];

        if(c->type == COMMAND_RETURN){
            ret_obj = apply_command(c, error);
            break;
        }

        else{
            apply_command(c, error);
            if(*error){
                return NULL;
            }
        }
    }

    // reset context
    context.m = cont_m;
    context.o = cont_o;

    // kill yourself
    // ... #imOnMyWay

    return ret_obj;
}

simle_obj* apply_command_object_method(command* cmd, int* error){
    printf("Thunk: ");
    int i;
    thunk* th = cmd->ptr[1];
    for(i=0; i<th->count; i++){
        printf("%s ", th->words[i]);
    }
    printf("\n");

    // check one word
        // check if var
    // find first var, go through syntax shit

    return NULL;
}

simle_obj* apply_command_var_method(command* cmd, int* error){
    simle_var* v = env_get_var(cmd->sm_method->block->env, cmd->ptr[0]);
    if(v == NULL){
        simle_err_set(runtime_error_var_not_found, cmd->ptr[0]);
        *error = 1;
        return NULL;
    }
    return v->obj;
}

simle_obj* apply_command_var_global(command* cmd, int* error){
    simle_var* v = env_get_var(mem->global_env, cmd->ptr[0]);
    if(v == NULL){
        simle_err_set(runtime_error_var_not_found, cmd->ptr[0]);
        *error = 1;
        return NULL;
    }
    return v->obj;
}

simle_obj* apply_command_var_class(command* cmd, int* error){
    simle_var* v = simle_obj_get_var(context.o, cmd->ptr[0]);
    if(v == NULL){
        simle_err_set(runtime_error_var_not_found, cmd->ptr[0]);
        *error = 1;
        return NULL;
    }
    return v->obj;
}
