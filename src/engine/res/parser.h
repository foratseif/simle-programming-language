#ifndef _PARSER_H_
#define _PARSER_H_

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <inttypes.h>
#include <ctype.h>

#include <res/parser/codeline.h>

codeline* parse_line(char* line);

#endif // _PARSER_H_

