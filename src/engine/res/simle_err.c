#include <res/simle_err.h>

char errbuff[SIMLE_ERR_MAXLEN];

// return buffer pointer
char* simle_err_get(void){
    return errbuff;
}

// add linenumber to error
void simle_err_set_lineno(int lineno){
    /* print line number */
    int len = strlen(errbuff);
    snprintf(errbuff+len, SIMLE_ERR_MAXLEN-len, " at line: %d", lineno);
    errbuff[SIMLE_ERR_MAXLEN-1] = '\0';
}

// set error
void simle_err_set(simle_err_type err, ... ){
    va_list arglist;
    va_start( arglist, err );

    int r = 0;

    // start printing error
    if(err >= runtime_error){
        r = snprintf(errbuff, SIMLE_ERR_MAXLEN, "RuntimeError:");
    }else{
        r = snprintf(errbuff, SIMLE_ERR_MAXLEN, "SyntaxError:");
    }


    /* check error type*/
    switch(err){
        case syntax_error_no_class_name:
            r += snprintf(errbuff+r, SIMLE_ERR_MAXLEN-r, " Expected class name");
            break;

        case syntax_error_no_ext_class_name:
            r += vsnprintf(errbuff+r, SIMLE_ERR_MAXLEN-r, " Expected name of the class that class %s will extending", arglist);
            break;

        case syntax_error_unexpected_word:
            r += vsnprintf(errbuff+r, SIMLE_ERR_MAXLEN-r, " Unexpected word '%s'", arglist);
            break;

        case syntax_error_unexpected_token:
            r += vsnprintf(errbuff+r, SIMLE_ERR_MAXLEN-r, " Unexpected token '%s'", arglist);
            break;

        case syntax_error_unexpected_indent:
            r+= snprintf(errbuff+r, SIMLE_ERR_MAXLEN-r, " Unexpected indent level");
            break;

        case syntax_error_premature_line_break:
            r+= snprintf(errbuff+r, SIMLE_ERR_MAXLEN-r, " Premature line break");
            break;

        case syntax_error_empty_path:
            r+= snprintf(errbuff+r, SIMLE_ERR_MAXLEN-r, " Empty path");
            break;

        case syntax_error_illegal_variable_name:
            r += vsnprintf(errbuff+r, SIMLE_ERR_MAXLEN-r, " Illegal variable name: %s", arglist);
            break;

        case syntax_error_already_used_once_in_method_def:
            r += vsnprintf(errbuff+r, SIMLE_ERR_MAXLEN-r, " Variable '%s' has already been used once in method definition", arglist);
            break;

        case syntax_error_wrong_first_var:
            r += vsnprintf(errbuff+r, SIMLE_ERR_MAXLEN-r, " First variable in method definition cannot be '%s'. It can either be '$THIS' in static context, or '$this' in non-static context", arglist);
            break;

        case syntax_error_class_variable_outside_class:
            r += vsnprintf(errbuff+r, SIMLE_ERR_MAXLEN-r, " Variable '%s' is not defined inside a class", arglist);
            break;

        case syntax_error_method_already_defined:
            r += snprintf(errbuff+r, SIMLE_ERR_MAXLEN-r, " Same method definition is used for another method in this class");
            break;

        case syntax_error_cant_interpret_line:
            r += snprintf(errbuff+r, SIMLE_ERR_MAXLEN-r, " Line could not be interpreted.");
            break;

        case syntax_error_incomplete_return_statement:
            r += snprintf(errbuff+r, SIMLE_ERR_MAXLEN-r, " Incomplete return statment");
            break;

        case syntax_error_var_name_empty:
            r += snprintf(errbuff+r, SIMLE_ERR_MAXLEN-r, " Empty variable name");
            break;

        case runtime_error:
            r += vsnprintf(errbuff+r, SIMLE_ERR_MAXLEN-r, " %s", arglist);
            break;

        case runtime_error_class_not_found:
            r += vsnprintf(errbuff+r, SIMLE_ERR_MAXLEN-r, " class '%s' has not been defined yet", arglist);
            break;

        case runtime_error_clib_late_set:
            r += snprintf(errbuff+r, SIMLE_ERR_MAXLEN-r, " _clib must be set the first time the class is created");
            break;

        case runtime_error_parent_late_set:
            r += snprintf(errbuff+r, SIMLE_ERR_MAXLEN-r, " class parent must be set the first time the class is created");
            break;

        case runtime_error_class_has_no_clib:
            r += vsnprintf(errbuff+r, SIMLE_ERR_MAXLEN-r, " class %s's _clib is not defined", arglist);
            break;

        case runtime_error_var_not_found:
            r += vsnprintf(errbuff+r, SIMLE_ERR_MAXLEN-r, " variable '%s' not found", arglist);
            break;

        case runtime_error_method_not_found:
            r += snprintf(errbuff+r, SIMLE_ERR_MAXLEN-r, " method not found.");
            break;

        default:
            r += snprintf(errbuff+r, SIMLE_ERR_MAXLEN-r, " LOOOL! Unknown error xD!!!!");
            break;
    }

    /* close error statement */
    errbuff[SIMLE_ERR_MAXLEN-1] = '\0';
    va_end( arglist );
}
