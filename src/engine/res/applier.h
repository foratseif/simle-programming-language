#ifndef _APPLIER_H_
#define _APPLIER_H_

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <inttypes.h>

// customs
#include <res/memory.h>
#include <runtime/env.h>
#include <res/interpreter/command.h>
#include <res/parser/thunk.h>
#include <lang/simle_obj.h>
#include <lang/simle_method.h>
#include <runtime/env.h>

void init_applier(simle_mem* m);
void quit_applier();

simle_obj* apply_command(command* cmd, int* error);

#endif // _APPLIER_H_
