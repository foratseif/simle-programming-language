#include <res/interpreter.h>

// pointer on simle simle_mem
simle_mem* mem;
list*      method_block_buffer;

// define private functions (code at bottom of file)
void clear_method_block_buffer();
int get_from_interpreter_stack(int indent, interpreter_object_type* type, void** ptr);
command* interpret_thunk(thunk* th, int* error, simle_method* sm_method);

// interpret a parsed line
command* interpret(codeline* cl, int* error){

    // clear error
    *error = 0;

    // ignore empty line
    if(cl->th->count == 0){
        return NULL;
    }

    // prepare fetching interpreter object
    void* ptr = NULL;
    interpreter_object_type type;

    // try fetching interpreter object
    if(get_from_interpreter_stack(cl->indent, &type, &ptr) != 0){
        *error = 1;
        return NULL;
    }

    if(ptr == NULL){
        // check indent syntax error for fomality
        if(cl->indent > 0){
            // set simle error to syntax_error_unexpected_indent
            simle_err_set(syntax_error_unexpected_indent);
            *error = 1;
            return NULL;
        }

        // check for class definition
        if(strcmp(cl->th->words[0], "class") == 0){

            // check codeline syntax

            int i=0;
            if(++i == cl->th->count){
                // set simle error to syntax_error_no_class_name
                simle_err_set(syntax_error_no_class_name);
                *error = 1;
                return NULL;
            }

            char* name   = cl->th->words[i];
            char* _clib  = NULL;
            char* extnds = NULL;

            i++;

            // check if there is more to match
            if(i < cl->th->count){
                if(strcmp(cl->th->words[i], "extends") == 0){

                    if(++i == cl->th->count){
                        simle_err_set(syntax_error_no_class_name);
                        *error = 1;
                        return NULL;
                    }

                    // get extention class
                    extnds = cl->th->words[i];

                    i++;
                }
            }

            // check if there is still more to match
            if(i < cl->th->count){

                if(strcmp(cl->th->words[i], "_clib") == 0){

                    if(++i == cl->th->count){
                        simle_err_set(syntax_error_premature_line_break);
                        *error = 1;
                        return NULL;
                    }

                    if(strcmp(cl->th->words[i], "=") != 0){
                        simle_err_set(syntax_error_unexpected_token, cl->th->words[i]);
                        *error = 1;
                        return NULL;
                    }

                    if(++i == cl->th->count){
                        simle_err_set(syntax_error_premature_line_break);
                        *error = 1;
                        return NULL;
                    }

                    if(strcmp(cl->th->words[i], "\"") != 0){
                        simle_err_set(syntax_error_unexpected_token, cl->th->words[i]);
                        *error = 1;
                        return NULL;
                    }

                    if(++i == cl->th->count){
                        simle_err_set(syntax_error_premature_line_break);
                        *error = 1;
                        return NULL;
                    }

                    if(strcmp(cl->th->words[i], "\"") == 0){
                        simle_err_set(syntax_error_empty_path);
                        *error = 1;
                        return NULL;
                    }else{
                        // set clib path relative to CWD
                        _clib = cl->th->words[i];
                    }

                    if(++i == cl->th->count){
                        simle_err_set(syntax_error_premature_line_break);
                        *error = 1;
                        return NULL;
                    }

                    if(strcmp(cl->th->words[i], "\"") != 0){
                        simle_err_set(syntax_error_unexpected_token, cl->th->words[i]);
                        *error = 1;
                        return NULL;
                    }

                    if(++i < cl->th->count){
                        simle_err_set(syntax_error_unexpected_token, cl->th->words[i]);
                        *error = 1;
                        return NULL;
                    }
                }
            }

            // check if there is still more to match
            if(i < cl->th->count){
                // else error
                simle_err_set(syntax_error_unexpected_word, cl->th->words[i]);
                *error = 1;
                return NULL;
            }

            // try to get class for memory
            simle_class* sm_class = memory_class_get(mem, name);

            // create class
            if(sm_class == NULL){

                // create class
                sm_class = new_simle_class(name);

                // if class extends another class
                if(extnds != NULL){
                    simle_class* parent = memory_class_get(mem, extnds);

                    // set parent if existing
                    if(parent != NULL){
                        sm_class->parent = parent;
                    }

                    // error, parent class not defined
                    else{
                        simle_err_set(runtime_error_class_not_found, extnds);
                        *error = 1;
                        return NULL;
                    }
                }

                //else, extends default object
                else {
                    //<CODE>
                }

                // add class to memory
                memory_class_add(mem, sm_class);

                // check if bridge is defined for class
                if(_clib != NULL){
                    // create bridge
                    bridge* b = bridge_create(_clib);

                    // if error while creating bridge
                    if(b == NULL){
                        *error = 1;
                        return NULL;
                    }

                    // add bridge to class
                    else{
                        sm_class->bridge = b;
                    }
                }

            }else{
                if(_clib != NULL){
                    simle_err_set(runtime_error_clib_late_set);
                    *error = 1;
                    return NULL;
                }
                if(extnds != NULL){
                    simle_err_set(runtime_error_parent_late_set);
                    *error = 1;
                    return NULL;
                }
            }

            // add to interpreter_stack
            interpreter_stack_add(cl->indent, CLASS, sm_class);
            return NULL;
        }
        else{
            // expecting regular command (interpret as thunk
            return interpret_thunk(cl->th, error, NULL);
        }
        return NULL;
    }else{

        // inside class
        if(type == CLASS){
            // get class
            simle_class*  sm_class  = (simle_class*) ptr;

            // check if method have been defined before
            if(simle_class_has_method_definition(sm_class, cl->th)){
                simle_err_set(syntax_error_method_already_defined);
                *error = 1;
                return NULL;
            }

            // create simle method
            simle_method* sm_method = new_simle_method(sm_class);
            sm_method->seq_c = cl->th->count;
            sm_method->seq_v = (char**) calloc(sizeof(char*), cl->th->count);

            // prep vars to loop through method definition
            int i;

            // loop through method definition
            for(i=0; i<cl->th->count; i++){
                // check if variable
                if(cl->th->words[i][0] == '$'){

                    // check for $this var
                    if(strcmp(cl->th->words[i], "$this") == 0){

                        // check if caller_pos is not defined
                        if(sm_method->caller_pos == -1){
                            sm_method->caller_pos = i;
                            sm_method->is_static = 0;
                        }

                        // else error
                        else{
                            //error, $this var already defined
                            simle_err_set(syntax_error_already_used_once_in_method_def, cl->th->words[sm_method->caller_pos]);
                            *error = 1;
                            return NULL;
                        }

                    }

                    // check for $THIS
                    else if(strcmp(cl->th->words[i], "$THIS") == 0){

                        // check if caller_pos is not defined
                        if(sm_method->caller_pos == -1){
                            sm_method->caller_pos = i;
                            sm_method->is_static = 1;
                        }

                        // else error
                        else{
                            //error, $this var already defined
                            simle_err_set(syntax_error_already_used_once_in_method_def, cl->th->words[sm_method->caller_pos]);
                            *error = 1;
                            return NULL;
                        }

                    }

                    // other var
                    else {

                        // check if this is not the first variable
                        if(sm_method->caller_pos > -1){
                            // dont need for: sm_method->seq[i] = NULL; because calloc is used

                            // check if params list is created
                            if(sm_method->params == NULL){
                                sm_method->params = list_new();
                            }

                            // add param to params list
                            char* param = (char*) calloc(sizeof(char), strlen(cl->th->words[i]) );
                            strcpy(param, cl->th->words[i]+1);
                            list_add(sm_method->params, param);
                        }

                        // else error: first variable has to be either $this or $THIS
                        else {
                            simle_err_set(syntax_error_wrong_first_var, cl->th->words[i]);
                            *error = 1;
                            return NULL;
                        }

                    }

                }

                // remember to check for stuff like string quotes or numbers (in heinsight: dont think i need to)

                // else syntax word
                else{
                    sm_method->seq_v[i] = (char*) calloc(sizeof(char), strlen(cl->th->words[i]) + 1 );
                    strcpy(sm_method->seq_v[i], cl->th->words[i]);
                }
            }

            // create buffer for list commands
            method_block_buffer = list_new();

            // keep track of it for freeing
            if(sm_method->is_static){
                simle_class_add_static_method(sm_class, sm_method);
            }else{
                simle_class_add_method(sm_class, sm_method);
            }

            // add method to stack
            interpreter_stack_add(cl->indent, METHOD, sm_method);
            return NULL;
        }

        // inside method
        if(type == METHOD){
            // get method
            simle_method* sm_method = (simle_method*) ptr;
            simle_class*  sm_class  = (simle_class*)  sm_method->sm_class;

            // prep command to go instide method
            command* cmd = NULL;

            // check if clib command
            if(strcmp(cl->th->words[0], "_clib") == 0){

                // start checking syntax
                if(cl->th->count >= 4){

                    // ... checking syntax
                    if(strcmp(cl->th->words[1], ".") == 0){
                        // get function name
                        char* name = cl->th->words[2];

                        // parse arguments and check syntax error
                        //<CODE>

                        // check if bridge is set
                        if(sm_class->bridge != NULL){
                            void* f = bridge_get_func(sm_class->bridge, name);
                            if(f == NULL){
                                *error = 1;
                                return NULL;
                            }

                            void* data[1];
                            data[0] = f;

                            cmd = create_command(COMMAND_CLIB, sm_method, sizeof(data)/sizeof(void*), data);

                            // add command to method block buffer
                            list_add(method_block_buffer, cmd);

                            return NULL;
                        }else{
                            simle_err_set(runtime_error_class_has_no_clib, sm_class->name);
                            *error = 1;
                            return NULL;
                        }
                    }else{
                        // error
                        simle_err_set(syntax_error_unexpected_token, cl->th->words[1]);
                        *error = 1;
                        return NULL;
                    }

                }

            }

            else if(strcmp(cl->th->words[0], "return") == 0){

                if(cl->th->count >= 2){
                    cmd = create_command(COMMAND_RETURN, sm_method, 0, NULL);

                    // add command to method block buffer
                    list_add(method_block_buffer, cmd);

                    return NULL;
                }else{
                    // error
                    simle_err_set(syntax_error_incomplete_return_statement);
                    *error = 1;
                    return NULL;
                }
            }

            // else interpret as thunk
            else{
                cmd = interpret_thunk(cl->th, error, sm_method);
                if(cmd != NULL){
                    list_add(method_block_buffer, cmd);
                }
                return NULL;
            }

            *error = 1;
            return NULL;
        }

        *error = 1;
        return NULL;
    }

    *error = 1;
    return NULL;
}

// prepare program for interpreting
void init_interpreter(simle_mem* m){
    // config stack
    interpreter_stack_init();

    mem = m;
}


// quit_interpreter
void quit_interpreter(){
    // let the interpreter_stack quit
    interpreter_stack_quit();
    clear_method_block_buffer();
}

// close off everything in current interpreter
int interpreter_close_all(){
    // prepare fetching interpreter object
    void* ptr = NULL;
    interpreter_object_type type;

    // try fetching interpreter object
    if(get_from_interpreter_stack(0, &type, &ptr) != 0){
        return 1;
    }

    return 0;
}



/* ===================================== PRIVATE FUNCTIONS ===================================== */

// clear method_block_buffer list
void clear_method_block_buffer(){
    if(method_block_buffer == NULL)
        return;

    while(list_pop(method_block_buffer) != NULL);

    free(method_block_buffer);
    method_block_buffer = NULL;
}

// gets objects from interpreter_stack and closes old objects
int get_from_interpreter_stack(int indent, interpreter_object_type* type, void** ptr){

    int get_status;
    while((get_status = interpreter_stack_get(indent, type, ptr)) != 0){

        // check if there are closed methods to compile
        if(get_status == 2){

            if(*type == METHOD){

                // create block for method out of buffer
                simle_block* block = simle_block_convert_list(method_block_buffer);

                //if(block == NULL){
                //    clear_method_block_buffer();
                //    return 1;
                //}

                // set block in method
                simle_method_set_block(*ptr, block);

                // set method buffer to NULL (cleared inside simle_block_convert_list function)
                method_block_buffer = NULL;

            }
        }else{
            // set simle error to syntax_error_unexpected_indent
            simle_err_set(syntax_error_unexpected_indent);
            return 1;
        }

        // reset pointer
        *ptr = NULL;
    }

    return 0;
}

// parse lise as regular command
command* interpret_thunk(thunk* th, int* error, simle_method* sm_method){

    // check if assignment
    if(th->count >= 3){
        if(strcmp(th->words[1], "=") == 0 &&
           (th->words[0][0] == '$' ||
            th->words[0][0] == '@')){

            // check if variable name is illegal
            if(strcmp(th->words[0], "$THIS") == 0 || strcmp(th->words[0], "$this") == 0){
                simle_err_set(syntax_error_illegal_variable_name, th->words[0]);
                *error = 1;
                return NULL;
            }

            if(sm_method == NULL && th->words[0][0] == '@'){
                simle_err_set(syntax_error_illegal_variable_name, th->words[0]);
                *error = 1;
                return NULL;
            }

            // create data arr
            void* data[2];

            // interpret the assignment object
            thunk* subthunk = sub_thunk(th, 2, th->count);              // subthunk for assignment value
            command* cmd = interpret_thunk(subthunk, error, sm_method); // turn subthunk to command
            free_thunk(subthunk);                                       // free subthunk

            // check for error
            if(*error){
                return NULL;
            }

            // add variable name to data
            data[0] = malloc(sizeof(char)*(strlen(th->words[0]) + 1));
            strcpy(data[0], th->words[0]);

            // add command to data
            data[1] = cmd;

            return create_command(COMMAND_ASSIGNMENT, sm_method, 2, data);
        }
    }

    if(th->count == 3){
        if(strcmp(th->words[0], "\"") == 0 &&
           strcmp(th->words[1], "\"") == 1){
            // SPECIAL_CONSTRUCTOR string
            printf("detected string\n");
        }
    }

    if(th->count == 1){
        char* x = th->words[0];
        while(*x != '\0'){
            if(*x < '0' || *x > '9'){
                break;
            }
            x++;
        }

        if(*x == '\0'){
            // SPECIAL_CONSTRUCTOR int
            printf("detected int\n");
        }else if(strcmp(th->words[0], "NULL") == 0){
            // SPECIAL_CONSTRUCTOR null!
            printf("detected int\n");
        }
    }

    // check if variable
    if(th->count == 1){
        void* data[1];

        // check if dollar-variable
        if(th->words[0][0] == '$'){

            // check if word is longer than 1 char
            if(th->words[0][1]){
                if(sm_method != NULL){
                    data[0] = malloc(strlen(th->words[0])*sizeof(char));
                    strcpy(data[0], th->words[0]+1);
                    return create_command(COMMAND_VAR_METHOD, sm_method, 1, data);
                }else{
                    data[0] = malloc(strlen(th->words[0])*sizeof(char));
                    strcpy(data[0], th->words[0]+1);
                    return create_command(COMMAND_VAR_GLOBAL, sm_method, 1, data);
                }
            }

            // syntax error no variable name provided
            else{
                simle_err_set(syntax_error_var_name_empty);
                *error = 1;
                return NULL;
            }
        }

        // check if at-variable
        if(th->words[0][0] == '@'){

            // check if word is longer than 1 char
            if(th->words[0][1]){
                if(sm_method != NULL){
                    data[0] = malloc(strlen(th->words[0])*sizeof(char));
                    strcpy(data[0], th->words[0]+1);
                    return create_command(COMMAND_VAR_CLASS, sm_method, 1, data);
                }else{
                    simle_err_set(syntax_error_class_variable_outside_class, th->words[0]);
                    *error = 1;
                    return NULL;
                }
            }

            // syntax error no variable name provided
            else{
                simle_err_set(syntax_error_var_name_empty);
                *error = 1;
                return NULL;
            }
        }
    }

    // check if if-statement
    // check if while-statement
    // check if try-catch

    // else loop through line try to find object (object is a variable) or class
    unsigned long i;
    simle_class* c = NULL;
    for(i=0; i<th->count; i++){

        // check if creating object of class
        if(sm_method != NULL && strcmp(th->words[i], "_object_") == 0){
            return create_command(COMMAND_NEW_OBJECT, sm_method, 0, NULL);
        }

        // check if word is class
        else if((c = memory_class_get(mem, th->words[i])) != NULL){
            break;
        }

        // check for variable
        if(th->words[i][0] == '$' || th->words[i][0] == '@'){

            // check if variable name is empty
            if(th->words[i][1] == '\0'){
                simle_err_set(syntax_error_var_name_empty);
                *error = 1;
                return NULL;
            }

            // if variable is $THIS which points to class
            if(strcmp(th->words[i], "$THIS") == 0){

                // error if $THIS is used outside class
                if(sm_method == NULL){
                    simle_err_set(syntax_error_illegal_variable_name, th->words[i]);
                    *error = 1;
                    return NULL;
                }


                // try to find static method in class
                list* th_list = list_new();
                simle_method* m = simle_class_static_method(sm_method->sm_class, th, i, th_list);

                if(m == NULL){
                    simle_err_set(syntax_error_cant_interpret_line);
                    *error = 1;
                    return NULL;
                }

                // create data arr to attach to command
                int data_size = th_list->size;
                void* data[data_size];

                // loop through thunk_list and interpret thunks to commands
                int data_index=0;
                thunk* param_th;
                foreach_clear(param_th, th_list){
                    // interpret thunk
                    data[data_index++] = interpret_thunk(param_th, error, sm_method);

                    // check for error
                    if(*error){
                        return NULL;
                    }

                    // free thunk
                    free_thunk(param_th);
                }

                // return command with type: COMMAND_STATIC_METHOD
                return create_command(COMMAND_STATIC_METHOD, m, data_size, data);
            }

            // else variable is a an object
            else{


                // check if its a class variable called outside a method (ERROR)
                if(th->words[i][0] == '@' && sm_method == NULL){
                    simle_err_set(syntax_error_class_variable_outside_class, th->words[i]);
                    *error = 1;
                    return NULL;
                }


                // todo(i think): should check if variable is set

                // else variable is an object, so create a command accordingly
                void* data[2];
                data[0] = (void*)i; // (i is casted to a pointer even though the literal value of i will be used)
                data[1] = clone_thunk(th);
                return create_command(COMMAND_OBJECT_METHOD, sm_method, 2, data);
            }
        }
    }

    // TODO: check for constructor syntaxes!

    // no word in entire codeline is a class
    if(c == NULL){
        simle_err_set(syntax_error_cant_interpret_line);
        *error = 1;
        return NULL;
    }

    // try to find static method in class
    list* th_list = list_new();
    simle_method* m = simle_class_static_method(c, th, i, th_list);
    if(m == NULL){
        simle_err_set(syntax_error_cant_interpret_line);
        *error = 1;
        return NULL;
    }

    // create data arr to attach to command
    int data_size = th_list->size;
    void* data[data_size];

    // loop through thunk_list and interpret thunks to commands
    int data_index=0;
    thunk* param_th;
    foreach_clear(param_th, th_list){
        // interpret thunk
        data[data_index++] = interpret_thunk(param_th, error, sm_method);

        // check for error
        if(*error){
            return NULL;
        }

        // free thunk
        free_thunk(param_th);
    }

    // return static method command
    return create_command(COMMAND_STATIC_METHOD, m, data_size, data);
}
