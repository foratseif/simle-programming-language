#ifndef _COMMAND_H_
#define _COMMAND_H_

// typedef structs
typedef enum   command_type_enum command_type;
typedef struct command_st        command;

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

// custom includes
#include <lang/simle_method.h>
#include <res/parser/thunk.h>


// structs
enum command_type_enum {
    COMMAND_ASSIGNMENT,
    COMMAND_IF,
    COMMAND_WHILE,
    COMMAND_CLIB,
    COMMAND_STATIC_METHOD,
    COMMAND_OBJECT_METHOD,
    COMMAND_YIELD,
    COMMAND_RETURN,
    COMMAND_NEW_OBJECT,
    COMMAND_VAR_METHOD,
    COMMAND_VAR_GLOBAL,
    COMMAND_VAR_CLASS
};

struct command_st {
    command_type  type;
    simle_method* sm_method;
    int           size;
    void*         ptr[];
};

// function definitions
command* create_command(command_type t, simle_method* m, int size, void* ptr[]);
void free_command(command* cmd);

// debug function(s)
void debug_print_command_type(enum command_type_enum t);

#endif // _COMMAND_H_
