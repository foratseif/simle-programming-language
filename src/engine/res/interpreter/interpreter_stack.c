#include "interpreter_stack.h"

//variables
static stack* interpreter_stack = NULL; // stack for code blocks

// initialize and prepare variables
void interpreter_stack_init(){
    interpreter_stack = stack_new();
}

// pop the top object off the stack
int interpreter_stack_pop(interpreter_object_type* type, void** ptr){
    interpreter_obj* tmp = stack_pop(interpreter_stack);
    if(tmp != NULL){
        *type = tmp->type;
        *ptr  = tmp->ptr;
        free(tmp);
        return 1;
    }else{
        *ptr  = NULL;
        return 0;
    }

    return 0;
}

// function definitions
// free allocated variables
void interpreter_stack_quit(){
    if(interpreter_stack != NULL){

        // free stack
        interpreter_obj* io;
        while((io = stack_pop(interpreter_stack)) != NULL){
            free(io);
        }

        free(interpreter_stack);
    }
}

// add an object to the interpreter stack
void interpreter_stack_add(uint8_t baseindent, interpreter_object_type type, void* ptr){
    // create interpreter object
    interpreter_obj* io = (interpreter_obj*) malloc(sizeof(interpreter_obj));

    // set values
    io->baseindent = baseindent;
    io->indent     = baseindent;
    io->type       = type;
    io->ptr        = ptr;

    // push on stack
    stack_push(interpreter_stack, io);
}

// get an object from the interpreter stack based on indent
int interpreter_stack_get(uint8_t indent, interpreter_object_type* type, void** ptr){

    interpreter_obj* tmp;

    if((tmp = stack_top(interpreter_stack)) != NULL){

        /* If current indent is less than the baseindent
         * of the interpreter object, then the interpreter
         * object is done and should be closed. */
        if(indent <= tmp->baseindent){
            stack_pop(interpreter_stack);
            *type = tmp->type;
            *ptr  = tmp->ptr;
            free(tmp);
            return 2;
        }

        // Else: the current indent is larger than the base indent
        else{
            if(tmp->indent < tmp->baseindent){
                printf("LOGIC ERROR!\n");
                return 0;
            }

            // check if indent is not set
            if(tmp->indent == tmp->baseindent){
                // set indent level
                tmp->indent = indent;
            }

            // check if the supplied indent is unexpected
            else if(indent != tmp->indent){
                simle_err_set(syntax_error_unexpected_indent, 0);
                return 1;
            }

            // return information here
            *type = tmp->type;
            *ptr  = tmp->ptr;
            return 0;
        }

    }

    return 0;
}
