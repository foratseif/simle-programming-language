#ifndef _INTRPTR_OBJ_H_
#define _INTRPTR_OBJ_H_

#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>

// customs
#include <tools/stack.h>
#include <res/simle_err.h>

// interpreter object types
typedef enum {
    CLASS,
    METHOD
} interpreter_object_type;

// interpreter object struct
typedef struct {
    uint8_t                  indent;
    uint8_t                  baseindent;
    interpreter_object_type  type;
    void*                    ptr;
} interpreter_obj;

// function definitions
void interpreter_stack_quit();
void interpreter_stack_init();
void interpreter_stack_add(uint8_t baseindent, interpreter_object_type type, void* ptr);
int  interpreter_stack_get(uint8_t indent, interpreter_object_type* type, void** ptr);
int  interpreter_stack_pop(interpreter_object_type* type, void** ptr);

#endif // _INTRPTR_OBJ_H_
