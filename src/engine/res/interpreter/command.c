#include "command.h"

// create command struct
command* create_command(command_type t, simle_method* m, int size, void* ptr[]){
    command* i = (command*) malloc(sizeof(command) + (sizeof(void*) * size));
    i->type = t;
    i->size = size;
    i->sm_method = m;
    memcpy(i->ptr, ptr, size*sizeof(void*));
    return i;
}

// free command
void free_command(command* cmd){
    // free pre allocated dat'
    /*
    if(cmd->type == COMMAND_OBJECT_METHOD){
        free_thunk(cmd->ptr[1]);
    } else if(cmd->type == COMMAND_ASSIGNMENT){
        free_thunk(cmd->ptr[0]);
    }*/

    printf("memory loss on free_command()\n");
    free(cmd);
}


/* ===================================== DEBUG FUNCTIONS ===================================== */
void debug_print_command_type(enum command_type_enum t){
    switch(t){
        case COMMAND_ASSIGNMENT: printf("COMMAND_ASSIGNMENT"); break;
        case COMMAND_IF: printf("COMMAND_IF"); break;
        case COMMAND_WHILE: printf("COMMAND_WHILE"); break;
        case COMMAND_CLIB: printf("COMMAND_CLIB"); break;
        case COMMAND_YIELD: printf("COMMAND_YIELD"); break;
        case COMMAND_STATIC_METHOD: printf("COMMAND_STATIC_METHOD"); break;
        case COMMAND_OBJECT_METHOD: printf("COMMAND_OBJECT_METHOD"); break;
        case COMMAND_RETURN: printf("COMMAND_RETURN"); break;
        case COMMAND_NEW_OBJECT: printf("COMMAND_NEW_OBJECT"); break;
        case COMMAND_VAR_METHOD: printf("COMMAND_VAR_METHOD"); break;
        case COMMAND_VAR_GLOBAL: printf("COMMAND_VAR_GLOBAL"); break;
        case COMMAND_VAR_CLASS: printf("COMMAND_VAR_CLASS"); break;
        default: printf("UNKNOWN"); break;
    }
}
