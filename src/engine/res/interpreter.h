#ifndef _INTERPRETER_H_
#define _INTERPRETER_H_


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <inttypes.h>
#include <ctype.h>

// customs
#include <tools/stack.h>
#include <tools/list.h>
#include <res/simle_err.h>
#include <res/memory.h>
#include <res/parser.h>
#include <res/bridge.h>
#include <res/interpreter/interpreter_stack.h>
#include <res/interpreter/command.h>
#include <res/parser/thunk.h>
#include <lang/simle_class.h>

void init_interpreter(simle_mem* m);
void quit_interpreter();

command* interpret(codeline* cl, int* error);
int      interpreter_close_all();

#endif // _INTERPRETER_H_
