#ifndef _BRIDGE_H_
#define _BRIDGE_H_

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <dlfcn.h>

// customs
#include <res/simle_err.h>

typedef struct {
    void* handler;
} bridge;

bridge* bridge_create(char* path);
int     bridge_call(bridge* b, char* method, void* args[]);
void    bridge_free(bridge* b);

void    bridge_call_func(void* f, void* args[]);
void*   bridge_get_func(bridge* b, char* name);

#endif // _BRIDGE_H_
