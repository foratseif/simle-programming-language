#include <stdlib.h>
#include <stdio.h>

#include <tools/rfile.h>
#include <res/parser.h>
#include <res/interpreter.h>

void print_command(command* cmd){

    printf("command: \n");

    printf("   type: ");
    debug_print_command_type(cmd->type);
    printf("\n");

    printf("   size: %d\n", cmd->size);
    printf("\n");

}

int main(int argc, char* argv[]){
    char* x;

    // check args
    if(argc < 2){
        x = "simle/Test.sm";
    }else{
        x = argv[1];
    }


    rfile* rf = rfile_new(x);

    if(rf == NULL){
        fprintf(stderr, "Error: %s\n", strerror(errno));
        return 1;
    }

    // initialize simle_mem
    simle_mem* main_memory = init_memory();

    // prepare interpreter
    init_interpreter(main_memory);

    // integer to check for errors
    int error = 0;

    // loop through lines, parse and interpret
    char* line;
    while((line = next_line(rf)) != NULL){

        // parse line
        codeline* parsed = parse_line(line);

        // interpret line
        command* cmd = interpret(parsed, &error);

        if(cmd != NULL && error == 0){
            print_command(cmd);
            free_command(cmd);
        }

        // free codeline
        free_codeline(parsed);

        // check if line needed more simle_mem
        if(READER_ALLOCD(line)){
            free(line);
        }

        // check for error
        if(error == 1){
            printf("%s\n", simle_err_get());
            printf("    at file: '%s'\n", rf->filepath);
            printf("    at line: %d\n", rf->lineno);

            break;
        }

    }
    // close open stuff inside interpreter
    interpreter_close_all();

    // free rfile struct
    rfile_free(rf);

    // quit interpreter
    quit_interpreter();

    // dump memory
    dump_memory(main_memory);

    // clear simle_mem
    clear_memory(main_memory);

    return 0;
}
