#include <stdio.h>
#include <stdlib.h>

// curstom includes
#include <tools/stack.h>

int main(void){
    char* a = "a";
    char* b = "bb";
    char* c = "ccc";

    stack* s = stack_new();
    printf("stack top: %s\n", (char*)stack_top(s));

    stack_push(s, a);
    printf("stack top: %s\n", (char*)stack_top(s));

    stack_push(s, b);
    printf("stack top: %s\n", (char*)stack_top(s));

    stack_push(s, c);
    printf("stack top: %s\n", (char*)stack_top(s));

    while(stack_top(s) != NULL){
        printf("stack pop: %s\n", (char*)stack_pop(s));
    }

    free(s);
    return 0;
}
