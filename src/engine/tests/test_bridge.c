#include <stdlib.h>
#include <stdio.h>
#include <errno.h>

#include <res/bridge.h>
#include <res/simle_err.h>

int main(void){

    // init bridge
    bridge* b =  bridge_create("src/engine/tests/test_bridge/test.so");

    if(b == NULL){
        printf("%s\n", simle_err_get());
        return EXIT_FAILURE;
    }

    // call function
    int i, j, k;
    i=3;
    j=5;
    k=0;

    bridge_call(b, "test", (void*[3]){&i, &j, &k});

    printf("sum is: %d\n", k);

    // clear bridge
    bridge_free(b);

    return 0;
}
