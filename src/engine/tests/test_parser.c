#include <stdlib.h>
#include <stdio.h>

#include <tools/rfile.h>
#include <res/parser.h>

int main(int argc, char* argv[]){
    char* x;

    // check args
    if(argc < 2){
        x = "simle/Test.sm";
    }else{
        x = argv[1];
    }


    rfile* rf = rfile_new(x);

    if(rf == NULL){
        fprintf(stderr, "Error: %s\n", strerror(errno));
        return 1;
    }

    char* line;
    while((line = next_line(rf)) != NULL){

        codeline* parsed = parse_line(line);

        // print out what has been parsed
        int i;
        printf("PARSED: %2d | %2d |", parsed->th->count, parsed->indent);
        for(i=0; i<parsed->th->count; i++){
            printf(" [%s]", parsed->th->words[i]);
        }
        printf("\n");

        free_codeline(parsed);

        if(READER_ALLOCD(line)){
            free(line);
        }
    }

    rfile_free(rf);

    return 0;
}
