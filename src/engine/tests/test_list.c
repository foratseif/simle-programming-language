#include <stdio.h>
#include <stdlib.h>

// curstom includes
#include <tools/list.h>

void print_list(list* l){
    printf("list size: %d\n", l->size);
    printf("list head: %s\n", (char*)list_head(l));
    printf("list tail: %s\n", (char*)list_tail(l));
    printf("list loop: \n");

    char* tmp;
    foreach(tmp, l){
        printf("  %s\n", tmp);
    }
    printf("\n");
}

int main(void){
    char* a = "a";
    char* b = "bb";
    char* c = "ccc";

    list* l = list_new();
    print_list(l);

    list_add(l, a);
    print_list(l);

    list_add(l, b);
    print_list(l);

    list_add(l, c);
    print_list(l);

    printf("\n == CLEARING LIST == \n \n");

    char* tmp;
    foreach_clear(tmp, l){
        printf("removing: %s\n", tmp);
        print_list(l);
    }

    print_list(l);

    free(l);
    return 0;
}
