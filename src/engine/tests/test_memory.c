#include <stdlib.h>
#include <stdio.h>

#include <res/memory.h>
#include <lang/simle_class.h>

int main(void){

    // init mem
    simle_mem* mem = init_memory();

    // create test class
    simle_class* a = new_simle_class("gogi");
    memory_class_add(mem, a);

    // create another test class
    simle_class* b = new_simle_class("bogi");
    memory_class_add(mem, b);

    // try to get class
    printf("test 1: ");
    b = memory_class_get(mem, "bogi");
    if(b != NULL && strcmp(b->name, "bogi") == 0){
        printf("ok\n");
    }else{
        printf("fail!\n");
    }

    // try to get class
    printf("test 2: ");
    b = memory_class_get(mem, "gogi");
    if(b != NULL && strcmp(b->name, "gogi") == 0){
        printf("ok\n");
    }else{
        printf("fail!\n");
    }

    // clear mem
    clear_memory(mem);

    return 0;
}
