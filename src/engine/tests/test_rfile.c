#include <stdlib.h>
#include <stdio.h>

// customs
#include <tools/rfile.h>

int main(void){
    char* x = "test_rfile.c";

    rfile* rf = rfile_new(x);

    if(rf == NULL){
        fprintf(stderr, "Error: %s\n", strerror(errno));
        return 1;
    }

    char* line;
    while((line = next_line(rf)) != NULL){
        printf("%s\n", line);
        if(READER_ALLOCD(line)){
            free(line);
        }
    }
    rfile_free(rf);

    return 0;
}
