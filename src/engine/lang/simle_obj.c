#include "simle_obj.h"

// creates a simle objiable
simle_obj* simle_obj_create(simle_class* c){

    // alloc obj space
    simle_obj* o = (simle_obj*) malloc(sizeof(simle_obj));

    // set vars
    o->sm_class = c;
    o->vars = list_new();
    o->pointer_count = 0;

    // return obj
    return o;
}

// create or set simle object variable to another simle object
void simle_obj_set_var(simle_obj* obj, char* var_name, simle_obj* o){
    // check if var i already defined by going through varuable list
    simle_var* var = simle_obj_get_var(obj, var_name);

    // check if matches found
    if(var == NULL){
        // create new varible
        var = simle_var_create(var_name);

        // set variable value to parameter object
        simle_var_set(var, o);

        // add the new variable to variable list
        list_add(obj->vars, var);
    }else{
        // reassign variable
        var->obj = o;
    }
}

// get simle static class variable
simle_var* simle_obj_get_var(simle_obj* obj, char* var_name){
    // check if var i already defined by going through varuable list
    simle_var* var = NULL;
    foreach(var, obj->vars){
        if(strcmp(var->name, var_name) == 0){
            break;
        }
    }
    return var;
}

// clear simle objiable
void simle_obj_free(simle_obj* o){
    free(o);
}
