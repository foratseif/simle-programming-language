#ifndef _SIMLE_METHOD_H_
#define _SIMLE_METHOD_H_

typedef struct simle_method_st simle_method;

#include <stdlib.h>
#include <inttypes.h>

// customs
#include "simle_class.h"
#include "simle_block.h"

//method struct
struct simle_method_st{
    // method definition
    uint8_t seq_c;
    char**  seq_v;
    int     caller_pos;

    // flags
    uint8_t is_static:1;
    uint8_t filler:7;

    // params list
    list* params; // (string list of params) //

    // methods class
    simle_class* sm_class;

    // methods block
    simle_block* block;
};

simle_method* new_simle_method(simle_class* sm_class);
void simle_method_free(simle_method* m);
void simle_method_set_block(simle_method* m, simle_block* block);

#endif // _SIMLE_METHOD_H_
