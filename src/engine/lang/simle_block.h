#ifndef _SIMLE_BLOCK_H_
#define _SIMLE_BLOCK_H_

typedef struct simle_block_st simle_block;

#include <stdlib.h>
#include <inttypes.h>

// customs
#include <res/interpreter.h>
#include <res/interpreter/command.h>
#include <tools/list.h>
#include <runtime/env.h>

// block
struct simle_block_st{
    // environment for tracking variables
    simle_env* env;

    // commands
    uint16_t len;
    command* commands[];
};

//simle_block* new_simle_block(list* cmd_list);
simle_block* simle_block_convert_list(list* cmd_list);
void simle_block_free(simle_block* b);
void simle_block_init_env(simle_block* b);
void simle_block_clear_env(simle_block* b);

#endif // _SIMLE_BLOCK_H_
