#ifndef _SIMLE_CLASS_H_
#define _SIMLE_CLASS_H_

typedef struct simle_class_st simle_class;

#include <inttypes.h>
#include <stdlib.h>
#include <string.h>

// customs
#include <tools/list.h>
#include "simle_method.h"
#include "simle_var.h"
#include "simle_obj.h"
#include <res/bridge.h>
#include <res/parser.h>
#include <res/parser/thunk.h>
#include <res/parser/codeline.h>

#define simle_class_add_method(c, m) list_add(c->methods, m)
#define simle_class_add_static_method(c, m) list_add(c->s_methods, m)

//class struct
struct simle_class_st{
    char*        name;
    bridge*      bridge;

    list*        vars;

    list*        methods;
    list*        s_methods;
    simle_class* parent;
};

simle_class* new_simle_class(char* name);
void simle_class_free(simle_class* c);
simle_method* simle_class_method(simle_class* c, thunk* th, int caller_pos);
simle_method* simle_class_static_method(simle_class* c, thunk* th, int caller_pos, list* th_list);
void       simle_class_set_var(simle_class* c, char* var_name, simle_obj* o);
simle_var* simle_class_get_var(simle_class* c, char* var_name);
simle_method* simle_class_has_method_definition(simle_class* c, thunk* th);

#endif // _SIMLE_CLASS_H_
