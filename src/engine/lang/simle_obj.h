#ifndef _SIMLE_OBJECT_H_
#define _SIMLE_OBJECT_H_

typedef struct simle_obj_st simle_obj;

#include <inttypes.h>

// customs
#include "simle_var.h"
#include "simle_class.h"
#include <tools/list.h>

//object struct
struct simle_obj_st{
    simle_class* sm_class;
    list*        vars;
    int          pointer_count;
};

simle_obj* simle_obj_create(simle_class* c);
void simle_obj_free(simle_obj* v);
void       simle_obj_set_var(simle_obj* obj, char* var_name, simle_obj* o);
simle_var* simle_obj_get_var(simle_obj* obj, char* var_name);

#endif // _SIMLE_OBJECT_H_
