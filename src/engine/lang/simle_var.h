#ifndef _SIMLE_VAR_H_
#define _SIMLE_VAR_H_

typedef struct simle_var_st simle_var;

#include "simle_obj.h"

//var struct
struct simle_var_st{
    char* name;
    simle_obj* obj;
};

simle_var* simle_var_create(char* name);
void       simle_var_free(simle_var* v);
void simle_var_set(simle_var* v, simle_obj* o);

#endif // _SIMLE_VAR_H_
