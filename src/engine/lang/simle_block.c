#include "simle_block.h"

simle_block* simle_block_convert_list(list* cmd_list){
    // init var
    simle_block* b = (simle_block*) malloc(sizeof(simle_block) + (sizeof(command*) * cmd_list->size));

    // set length
    b->len = cmd_list->size;

    // set environment
    b->env = NULL; //init_env();

    /* copy commands from cmd_list to commands[] */
    // prep vars for copy
    int i = 0;
    command* cmd;

    // loop and copy
    foreach_clear(cmd, cmd_list){
        b->commands[i++] = cmd;
    }

    // remove the list base
    free(cmd_list);

    return b;
}

void simle_block_free(simle_block* b){
    // check if block is set
    if(b == NULL)
        return;

    if(b->env != NULL){
        clear_env(b->env);
    }

    if(b->commands != NULL){
        int i;
        for(i=0; i<b->len; i++){
            free_command(b->commands[i]);
        }
    }

    // free block
    free(b);
}


//simle_block* new_simle_block(list* cmd_list){
//    // init var
//    simle_block* b = (simle_block*) malloc(sizeof(simle_block));
//
//    return b;
//}

void simle_block_init_env(simle_block* b){
    // create environment
    b->env = init_env(NULL);
}

void simle_block_clear_env(simle_block* b){

    if(b->env != NULL){
        clear_env(b->env);
        b->env = NULL;
    }

}
