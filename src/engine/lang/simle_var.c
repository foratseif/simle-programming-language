#include "simle_var.h"

// creates a simle variable
simle_var* simle_var_create(char* name){

    // alloc var space
    simle_var* v = (simle_var*) malloc(sizeof(simle_var));

    // set name
    v->name = (char*) calloc(sizeof(char), strlen(name)+1);
    strcpy(v->name, name);

    // set object
    v->obj = NULL;

    // return var
    return v;
}

// sets variable value to object
void simle_var_set(simle_var* v, simle_obj* o){
    if(v->obj != NULL){
        if(--(v->obj->pointer_count) <= 0){
            simle_obj_free(v->obj);
        }
    }

    if(o != NULL){
        o->pointer_count++;
    }

    v->obj = o;
}

// clear simle variable
void simle_var_free(simle_var* v){

    if(v->obj != NULL){
        simle_obj_free(v->obj);
    }

    free(v);
}
