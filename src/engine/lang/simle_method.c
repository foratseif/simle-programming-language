#include "simle_method.h"

simle_method* new_simle_method(simle_class* sm_class){
    // init var
    simle_method* m = (simle_method*) malloc(sizeof(simle_method));

    // set class
    m->sm_class = sm_class;

    // set params list
    m->params = NULL;

    // prep vars
    m->block = NULL;
    m->is_static = 0;
    m->caller_pos = -1;

    return m;
}

void simle_method_set_block(simle_method* m, simle_block* block){
    m->block = block;
}

void simle_method_free(simle_method* m){
    // check if method is set
    if(m == NULL)
        return;

    // clear sequence
    if(m->seq_v != NULL){
        int i;
        for(i=0; i<m->seq_c; i++){
            if(m->seq_v[i] != NULL){
                free(m->seq_v[i]);
            }
        }
        free(m->seq_v);
    }

    // free block
    if(m->block != NULL){
        simle_block_free(m->block);
    }

    // free params list
    if(m->params != NULL){
        char* s;
        foreach_clear(s, m->params){
            free(s);
        }
    }

    // free method
    free(m);
}
