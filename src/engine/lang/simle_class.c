#include "simle_class.h"

int is_int(char* str);

simle_class* new_simle_class(char* name){
    // init var
    simle_class* c = (simle_class*) malloc(sizeof(simle_class));

    // add name
    c->name = (char*) calloc(strlen(name)+1, sizeof(char));
    strcpy(c->name, name);

    // clear other variables
    c->bridge = NULL;
    c->parent = NULL;

    // create methods and static methods list
    c->methods = list_new();
    c->s_methods = list_new();

    // create variable list
    c->vars = list_new();

    return c;
}


void simle_class_free(simle_class* c){

    // check if value is set
    if(c == NULL)
        return;

    // free name
    if(c->name != NULL){
        free(c->name);
    }

    // close and free bridge
    if(c->bridge != NULL){
        bridge_free(c->bridge);
    }

    // clear class methods
    if(c->methods != NULL){
        simle_method* m;
        foreach_clear(m, c->methods){
           simle_method_free(m);
        }
        free(c->methods);
    }

    // clear class static methods
    if(c->s_methods != NULL){
        simle_method* m;
        foreach_clear(m, c->s_methods){
           simle_method_free(m);
        }
        free(c->s_methods);
    }

    // clear class vars
    if(c->vars != NULL){
        simle_var* v;
        foreach_clear(v, c->vars){
           simle_var_free(v);
        }
        free(c->vars);
    }

    // free class
    free(c);
}

// find a static method in class by matching thunk
simle_method* simle_class_static_method(simle_class* c, thunk* th, int caller_pos, list* th_list){

    // search static mehtods in class
    simle_method* m;
    foreach(m, c->s_methods){

       int match = 1;

        // if caller position matches the class word
        if(m->caller_pos == caller_pos){

            // loop and match class sequence with thunk
            int x, y = 0;
            for(x=0; (x < th->count) && (y < m->seq_c); x++){

                // skip place of class name (caller_pos)
                if(x == caller_pos){
                    y++;
                    continue;
                }

                // check if place is NOT for variable
                if(m->seq_v[y] != NULL){
                    if(strcmp(m->seq_v[y], th->words[x]) == 0){

                    }else{
                        match = 0;
                        break;
                    }
                }else{
                    if(th->words[x][0] == '$'){
                        // check if a thunk list is provided
                        if(th_list){
                            // ad thunk of variable
                            list_add(th_list, sub_thunk(th, x, x+1));
                        }
                    }

                    // else if paranteses
                    else if(strcmp(th->words[x], "(") == 0){
                        x++;
                        int start = x;
                        for(; x < th->count; x++){
                            if(strcmp(th->words[x], ")") == 0){
                                // check if a thunk list is provided
                                if(th_list){
                                    // ad thunk of variable
                                    list_add(th_list, sub_thunk(th, start, x));
                                }
                                break;
                            }
                        }

                    }

                    // else if paranteses
                    else if(strcmp(th->words[x], "\"") == 0){
                        int start = x;
                        x++;
                        for(; x < th->count; x++){
                            if(strcmp(th->words[x], "\"") == 0){
                                // check if a thunk list is provided
                                if(th_list){
                                    // ad thunk of variable
                                    list_add(th_list, sub_thunk(th, start, x+1));
                                }
                                break;
                            }
                        }

                    }

                    else if(strcmp(th->words[x], "NULL") == 0){
                        // check if a thunk list is provided
                        if(th_list){
                            // ad thunk of variable
                            list_add(th_list, sub_thunk(th, x, x+1));
                        }
                    }

                    else if(is_int(th->words[x])){
                        // check if a thunk list is provided
                        if(th_list){
                            // ad thunk of variable
                            list_add(th_list, sub_thunk(th, x, x+1));
                        }
                    }

                    else{
                        printf("Expected variable but got '%s' instead\n", th->words[x]);
                        return NULL;
                    }
                }
                y++;
            }

            if(match && y == m->seq_c && x == th->count){
                return m;
            }
        }

    }

    return NULL;
}

// find a method in class by matching thunk
simle_method* simle_class_method(simle_class* c, thunk* th, int caller_pos){
    // search static mehtods in class
    simle_method* m;
    foreach(m, c->methods){

        int match = 1;

        // if caller position matches the class word
        if(m->caller_pos == caller_pos){

            // loop and match class sequence with codeline
            int x;
            for(x=0; (x < th->count) && (x < m->seq_c); x++){

                // skip place of object variable (caller_pos)
                if(x == caller_pos) continue;

                // check if place is NOT for variable
                if(m->seq_v[x] != NULL){
                    if(strcmp(m->seq_v[x], th->words[x]) == 0){
                        // word matched in sequence
                    }else{
                        match = 0;
                        break;
                    }
                }else{
                    if(th->words[x][0] == '$'){
                        // place is for variable
                    }else{
                        printf("Expected variable but got '%s' instead\n", th->words[x]);
                        return NULL;
                    }
                }
            }

            if(match && x == m->seq_c && x == th->count){
                return m;
            }

        }

    }

    return NULL;
}

simle_method* simle_class_has_method_definition(simle_class* c, thunk* th){

    simle_method* m = NULL;

    int i;
    for(i=0; i<th->count; i++){
        if(strcmp(th->words[i], "$THIS") == 0){
            m = simle_class_static_method(c, th, i, NULL);
            if(m != NULL){
                return m;
            }
        }else if(strcmp(th->words[i], "$this") == 0){
            m = simle_class_method(c, th, i);
            if(m != NULL){
                return m;
            }
        }
    }

    return m;
}

// create or set simle static class variable to simle object
void simle_class_set_var(simle_class* c, char* var_name, simle_obj* o){
    // check if var i already defined by going through varuable list
    simle_var* var = simle_class_get_var(c, var_name);

    // check if matches found
    if(var == NULL){
        // create new varible
        var = simle_var_create(var_name);

        // set variable value to parameter object
        simle_var_set(var, o);

        // add the new variable to variable list
        list_add(c->vars, var);
        //printf("(var '%s' created)", var_name);
    }else{
        // reassign variable
        var->obj = o;
        //printf("(var '%s' reset)", var_name);
    }
}

// get simle static class variable
simle_var* simle_class_get_var(simle_class* c, char* var_name){
    // check if var i already defined by going through varuable list
    simle_var* var = NULL;
    foreach(var, c->vars){
        if(strcmp(var->name, var_name) == 0){
            break;
        }
    }
    return var;
}

int is_int(char* str){
    while(*str != '\0'){
        if(*str > '9' || *str < '0'){
            return 0;
        }
        str++;
    }
    return 1;
}
