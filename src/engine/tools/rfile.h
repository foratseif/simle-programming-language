#ifndef _RFILE_H_
#define _RFILE_H_

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#ifndef BUFFER_SIZE
#define BUFFER_SIZE 82
#endif

#define READER_ALLOCD(x) (x != BUFFER)

char BUFFER[BUFFER_SIZE];

typedef struct rfile_struct {
    int lineno;
    FILE* file;
    char* filepath;
} rfile;

rfile* rfile_new(char* file);
void rfile_free(rfile* rf);
char* next_line(rfile* rf);

#endif // _RFILE_H_

