#include "stack.h"

// define node struct
struct stack_node_st {
    void* ptr;
    stack_node* prev;
};

// create new stack struct
stack* stack_new(){
    stack* s = (stack*) malloc(sizeof(stack));
    s->top = NULL;
    return s;
}

// pushes element to stack
void stack_push(stack* s, void* x){
    // create new top
    stack_node* new_top = (stack_node*) malloc(sizeof(stack_node));

    // set variables
    new_top->prev = s->top;
    new_top->ptr = x;

    // set new top
    s->top = new_top;
}

// returns top element of the stack
void*  stack_top(stack* s){
    if(s->top == NULL)
        return NULL;
    else
        return s->top->ptr;
}

// pops the top element of stack and returns it
void*  stack_pop(stack* s){
    if(s->top == NULL){
        return NULL;
    }else{
        // store return address
        void* ret = s->top->ptr;

        // remove top node and free memory
        stack_node* top = s->top;
        s->top = top->prev;
        free(top);

        // return address
        return ret;
    }
}
