#include "rfile.h"

// create new rfile struct
rfile* rfile_new(char* file){

    // open file
    FILE* f = fopen(file, "r");
    if(f == NULL)
        return NULL;

    // create rfile struct
    rfile* rf = (rfile*) calloc( 1, sizeof(rfile) );
    if(rf == 0)
        return NULL;

    // set FILE
    rf->file = f;

    // set filepath
    rf->filepath = calloc( strlen(file)+1 , sizeof(char) );
    if(rf->filepath == 0){
        rfile_free(rf);
        return NULL;
    }
    strcpy(rf->filepath, file);

    // return rfile struct
    return rf;
}

// frees rfile struct
void rfile_free(rfile* rf){
    // close file
    if(rf->file != NULL)
        fclose(rf->file);

    // free filepath
    if(rf->filepath != NULL)
        free(rf->filepath);

    // free rfile
    free(rf);
}

// gets the next line of file in rfile struct
char* next_line(rfile* rf){
    char* line;

    // setup a proper line
    line = fgets(BUFFER, BUFFER_SIZE, rf->file);

    // check if file is done
    if(line == NULL)
        return NULL;

    int len = strlen(line);
    rf->lineno++;

    // check if whole line or only part of a line
    if(line[len-1] == '\n'){
        // whole line
        line[len-1] = '\0';
    }else{
        // line longer than BUFFER_SIZE, need to alloc more mem

        // alloc double BUFFER_SIZE
        int times = 2;
        char* dynamic = calloc(BUFFER_SIZE*times, sizeof(char));
        strcpy(dynamic, line);

        // read more and realloc if havent reached line end
        while(1){
            line = fgets(BUFFER, BUFFER_SIZE, rf->file);
            len  = strlen(line);

            strcat(dynamic, line);

            if(line[len-1] == '\n'){
                // reached line end
                dynamic[(times-1)*BUFFER_SIZE + len - 2] = '\0';
                return dynamic;
            }else{
                // increase allocated memory
                times++;
                dynamic = realloc(dynamic, ( BUFFER_SIZE*times )*sizeof(char));
            }
        }

    }

    return line;
}






