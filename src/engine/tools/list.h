#ifndef _LIST_H_
#define _LIST_H_

#include <stdlib.h>
#include <stdio.h>

// define for foreach loop
#define foreach(x, lst)       for(x = list_loop_start(lst); x != NULL; x = list_loop_next(lst))
#define foreach_clear(x, lst) for(x = list_pop(lst); x != NULL; x = list_pop(lst))

// predefine typedef
typedef struct list_node_st list_node;

// list struct
typedef struct list_st {
    int size;
    list_node* head;
    list_node* tail;
    list_node* loop;
} list;

list*  list_new();
void   list_add(list* l, void* x);
void*  list_pop(list* l);
void*  list_head(list* l);
void*  list_tail(list* l);

void* list_loop_start(list* l);
void* list_loop_next(list* l);

#endif // _LIST_H_

