#ifndef _STACK_H_
#define _STACK_H_

#include <stdlib.h>
#include <stdio.h>

// predefine typedef
typedef struct stack_node_st stack_node;

// stack struct
typedef struct stack_st {
    stack_node* top;
} stack;

stack* stack_new();
void   stack_push(stack* s, void* x);
void*  stack_top(stack* s);
void*  stack_pop(stack* s);

#endif // _STACK_H_

