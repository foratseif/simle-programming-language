#include "list.h"

// define node struct
struct list_node_st {
    void* ptr;
    list_node* next;
};

// create new list struct
list* list_new(){
    list* l = (list*) malloc(sizeof(list));
    l->size = 0;
    l->head = NULL;
    l->tail = NULL;
    l->loop = NULL;
    return l;
}

// pushes element to list
void list_add(list* l, void* x){
    // create new top
    list_node* new_tail = (list_node*) malloc(sizeof(list_node));

    // set variables
    new_tail->next = NULL;
    new_tail->ptr = x;

    // set new tail
    if(l->tail != NULL){
        l->tail->next = new_tail;
    }

    // check if this is first node
    if(l->head == NULL){
        l->head = new_tail;
    }

    // point tail to new node
    l->tail = new_tail;

    // increment size
    l->size++;
}

// returns first element of the list
void* list_head(list* l){
    if(l->head == NULL)
        return NULL;
    else
        return l->head->ptr;
}

// returns last element of the list
void* list_tail(list* l){
    if(l->tail == NULL)
        return NULL;
    else
        return l->tail->ptr;
}

// pops the first element of list and returns it
void* list_pop(list* l){
    if(l->head == NULL){
        return NULL;
    }else{
        // store return address
        void* ret = l->head->ptr;

        // remove first node and free memory
        list_node* head = l->head;

        // check if there is only one element
        if(head == l->tail){
            l->tail = NULL;
        }

        // point list head to 2nd element
        l->head = head->next;

        // free first element
        free(head);

        // decrement size
        l->size--;

        // return address
        return ret;
    }
}

// initialize a list loop
void* list_loop_start(list* l){
    // if list is empty return NULL
    if(l->head == NULL){
        return NULL;
    }else{
        // init loop
        l->loop = l->head;

        // return pointer
        return l->loop->ptr;
    }
}

// find next element in loop, or return NULL
void* list_loop_next(list* l){
    if(l->loop->next == NULL){
        return NULL;
    }else{
        l->loop = l->loop->next;
        return l->loop->ptr;
    }
}

