#include "smart_mem.h"

smart_mem* TAIL = NULL;

int main(void){
    char* x;

    x = (char*) alloc_mem(5);
    strcpy(x, "gogi");
    smart_mem_print();

   /* x = (char*) alloc_mem(11);
    strcpy(x, "Forat Seif");
    smart_mem_print();

    x = (char*) alloc_mem(3);
    strcpy(x, "12");
    smart_mem_print();
*/
    smart_mem_clear();
    return 0;
}


void* alloc_mem(size_t size){
    smart_mem* sm = (smart_mem*) calloc(1, sizeof(smart_mem) + size);
    sm->mem = sm + sizeof(smart_mem);

    if(TAIL == NULL){
        TAIL = sm;
    }else{
        TAIL->next = sm;
        sm->prev = TAIL;
        TAIL = sm;
    }

    return sm->mem;
}

void free_mem(void* mem){
    smart_mem* sm = (smart_mem*) mem-sizeof(smart_mem);

    // remove sm from the linked list
    if(sm == TAIL){
        if(TAIL->prev == NULL){
            // sm is the only element that exist
            TAIL = NULL;
        }else{
            TAIL = sm->prev;
            TAIL->next = NULL;
        }
    }else{
        if(sm->prev != NULL){
            // element is in the middle
            sm->next->prev = sm->prev;
            sm->prev->next = sm->next;
        }else{
            // element is first
            sm->next->prev = NULL;
        }
    }

    free(sm);
}

void smart_mem_clear(){
    while(TAIL != NULL){
        free_mem(TAIL->mem);
    }
}

void smart_mem_print(){
    smart_mem* sm = TAIL;

    while(sm->prev != NULL){
        sm = sm->prev;
    }

    printf("\n\nprint smart mem:\n");
    while(sm != NULL){
        char tmp[26] = "                         ";
        int len = strlen(sm->mem);
        strncpy(tmp, sm->mem, len);
        printf("+-------------------------+\n");
        printf("|%s|\n", tmp);
        printf("+-------------------------+\n");
        sm = sm->next;

        if(sm != NULL){
            printf("             |             \n");
            printf("            \\|/            \n");
        }
    }
}
