#include <stdlib.h>
#include <stdio.h>
#include <string.h>

typedef struct smart_mem_st {
    void* mem;
    struct smart_mem_st* next;
    struct smart_mem_st* prev;
} smart_mem;

void* alloc_mem(size_t size);
void free_mem();
void smart_mem_clear();
void smart_mem_print();
