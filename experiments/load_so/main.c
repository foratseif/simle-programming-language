#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <dlfcn.h>

int main(void){

    void* handler = dlopen("./libgogi.so", RTLD_LAZY);
    char* (*funcptr)(void);

    if(handler != NULL){
        funcptr = dlsym(handler, "gogkii");
        if(funcptr == NULL){
            printf("err: %s\n", dlerror());
            return 1;
        }
        printf("string: %s\n", funcptr());
        dlclose(handler);
    }else{
        printf("failed!\n");
    }

    return 0;
}
