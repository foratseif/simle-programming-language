#define create_codeline(n, args...) facklife(n, (char*[n]){args})
codeline* facklife(int n, char* fack[]){
    codeline* cl = (codeline*) malloc(sizeof(codeline));
    cl->count = n;
    cl->indent = 0;
    cl->words = (char**) malloc(cl->count * sizeof(char*));

    int i;
    for(i=0; i<n; i++){
        cl->words[i] = (char*) calloc(sizeof(char), (1+strlen(fack[i])));
        strcpy(cl->words[i], fack[i]);
    }
    return cl;
}
