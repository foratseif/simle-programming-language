#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

enum syntnode_type {
    SYNTAX_WORD,
    SYNTAX_VAR,
    SYNTAX_OBJ
};

typedef struct {
    enum syntnode_type type;
    void*              ptr;
} syntnode;

void print_indent(int x){
    int i;
    for(i=0; i<x; i++){
        printf(" ");
    }
}

int match_syntax(syntnode* node, char* words[]){

    int matches = 0;

    // loop through node array
    while(node->type != SYNTAX_END){
        char* word = words[matches];

        // check for word
        if(node->type == SYNTAX_WORD){

            // match word
            if(strcmp(word, node->ptr) == 0){
                matches++;
            }

            // match fail
            else{
                return matches;
            }

        }

        // check for var
        else if(node->type == SYNTAX_VAR){
            matches ++;
        }

        // check for nested
        else if(node->type == SYNTAX_NESTED){
            int m = match_syntax(node->ptr, words+matches);

        }

    }

    return matches;

}

int syntax(codeline* cl){
    syntnode string_syntax[] = {
        {SYNTAX_WORD, "\""},
        {SYNTAX_VAR,  NULL},
        {SYNTAX_WORD, "\""},
        {SYNTAX_END,  NULL}
    };

    syntnode class[] = {
        {SYNTAX_WORD,  "class"},
        {SYNTAX_VAR,   NULL},
        {SYNTAX_GHOST, (syntnode[]){
            {SYNTAX_WORD, "extends"},
            {SYNTAX_VAR,  NULL},
            {SYNTAX_END, NULL}
        }},
        {SYNTAX_GHOST, (syntnode[]){
            {SYNTAX_WORD,   "_clib"},
            {SYNTAX_WORD,   "="},
            {SYNTAX_NESTED, string_syntax},
            {SYNTAX_END,    NULL}
        }},
        {SYNTAX_END,  NULL}
    };



    return 0;
}
