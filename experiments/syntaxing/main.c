#include <stdlib.h>
#include <stdio.h>

#include "../../src/engine/res/parser.h"

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

// SPECS:
// - match sequences and variables
// - (loops)
// - (may exist)

enum syntnode_type {
    SYNTAX_WORD,
    SYNTAX_VAR,
    SYNTAX_LOOP,
    SYNTAX_GHOST,
    SYNTAX_NESTED,
    SYNTAX_SEQ,
    SYNTAX_END
};

typedef struct {
    enum syntnode_type type;
    void*              ptr;
} syntnode;

void  print_syntax(syntnode* syntarr){

    int i, br = 0;
    for(i=0; br == 0; i++){
        syntnode* node = (syntarr+i);

        switch(node->type){
            case SYNTAX_WORD:
                printf("%s%s%s ", ANSI_COLOR_CYAN, (char*) node->ptr, ANSI_COLOR_RESET);
                break;

            case SYNTAX_VAR:
                printf("%s$var%s ", ANSI_COLOR_RED, ANSI_COLOR_RESET);
                break;

            case SYNTAX_NESTED:
                printf("%snesting-start-(%s ", ANSI_COLOR_YELLOW, ANSI_COLOR_RESET);
                    print_syntax(node->ptr);
                printf("%s)-nesting-end%s ", ANSI_COLOR_YELLOW, ANSI_COLOR_RESET);
                break;

            case SYNTAX_LOOP:
                printf("%sloop-start-(%s ", ANSI_COLOR_MAGENTA, ANSI_COLOR_RESET);
                    print_syntax(node->ptr);
                printf("%s)-loop-end%s ", ANSI_COLOR_MAGENTA, ANSI_COLOR_RESET);
                break;

            case SYNTAX_GHOST:
                printf("%sghost-start-(%s ", ANSI_COLOR_GREEN, ANSI_COLOR_RESET);
                    print_syntax(node->ptr);
                printf("%s)-ghost-end%s ", ANSI_COLOR_GREEN, ANSI_COLOR_RESET);
                break;

            case SYNTAX_END:
                br = 1;
                break;
        }
    }

}

int match_syntax(syntnode* syntarr, codeline* cl, int offset){

    syntnode* node = syntarr;

    int i, matches=offset;
    for(i=0; ; i++){
        char* word = cl->words[matches];
        node = (syntarr+i);

        if(node->type == SYNTAX_END){
            // ERROR unexpected word!!!
            if(offset == 0){
                printf("  ERROR: unexpected word: '%s'\n", word);
            }
            return matches;
        }

        else if(node->type == SYNTAX_WORD){
            if(strcmp(word, node->ptr) == 0){
                // match
                printf("  match: %s\n", (char*)node->ptr);
                matches++;
            }else{
                // words dont match
                printf("  ERROR: unexpected word: '%s'.\n", word);
                printf("  Expected: '%s'\n", (char*)node->ptr);
                return matches;
            }
        }

        else if(node->type == SYNTAX_VAR){
            printf("  var: '%s'\n", word);
            matches++;
        }

        else if(node->type == SYNTAX_NESTED){
            printf("  nesting start:\n");
            matches += match_syntax(node->ptr, cl, matches);
            printf("  nesting end\n");
        }

        else if(node->type == SYNTAX_GHOST){
            printf("  nesting start:\n");
            matches += match_syntax(node->ptr, cl, matches);
            printf("  nesting end\n");
        }


        else{
            printf("  UNDEFINED\n");
        }

        if(matches == cl->count){
            break;
        }
    }

    if(matches == cl->count){
        printf("  Match successful\n");
        // correct end
    }else{
        printf("  ERROR: expected more words!\n");
        // ERROR expected more word!
    }

    return matches;
}

#define create_codeline(n, args...) facklife(n, (char*[n]){args})
codeline* facklife(int n, char* fack[]){
    codeline* cl = (codeline*) malloc(sizeof(codeline));
    cl->count = n;
    cl->indent = 0;
    cl->words = (char**) malloc(cl->count * sizeof(char*));

    int i;
    for(i=0; i<n; i++){
        cl->words[i] = (char*) calloc(sizeof(char), (1+strlen(fack[i])));
        strcpy(cl->words[i], fack[i]);
    }
    return cl;
}

int main(void){

    printf("%scyan%s    = word \n", ANSI_COLOR_CYAN, ANSI_COLOR_RESET);
    printf("%sred%s     = variable \n", ANSI_COLOR_RED, ANSI_COLOR_RESET);
    printf("%syellow%s  = nested \n", ANSI_COLOR_YELLOW, ANSI_COLOR_RESET);
    printf("%smagenta%s = loop \n", ANSI_COLOR_MAGENTA, ANSI_COLOR_RESET);
    printf("%sgreen%s   = ghost \n", ANSI_COLOR_GREEN, ANSI_COLOR_RESET);
    printf("\n");

    codeline* cl;

    // ----
    static syntnode string_syntax[4] = {
        {SYNTAX_WORD, "\""},
        {SYNTAX_VAR,  NULL},
        {SYNTAX_WORD, "\""},
        {SYNTAX_END,  NULL}
    };

    printf("#STRING: \n");

    cl = create_codeline(4, "\"", "faaaaaaaaaaack", "\"", "shit");
    match_syntax(string_syntax, cl, 0);

    printf("\n");
    cl = create_codeline(3, "\"", "faaaaaaaaaaack", "\"");
    match_syntax(string_syntax, cl, 0);

    printf("\n\n");

    // ----
    printf("#ASSIGNMENT:\n");

    cl = create_codeline(2, "a", "b");

    match_syntax((syntnode[4]){
            {SYNTAX_VAR,  NULL},
            {SYNTAX_WORD,  "="},
            {SYNTAX_VAR,  NULL},
            {SYNTAX_END,  NULL}
    }, cl, 0);

    printf("\n\n");

    // ----
    printf("#NESTED STRING INSIDE IF STATEMENT:\n");

    cl = create_codeline(5, "if", "\"", "fackme", "\"", "do");

    match_syntax((syntnode[4]){
            {SYNTAX_WORD,  "if"},
            {SYNTAX_NESTED, string_syntax},
            {SYNTAX_WORD,  "do"},
            {SYNTAX_END,  NULL}
    }, cl, 0);

    printf("\n\n");

    // ----
    printf("#ARRAY SYNTAX:\n");

    cl = create_codeline(2, "a", "b");

    match_syntax((syntnode[4]){
        {SYNTAX_WORD,  "["},
        {SYNTAX_GHOST, (syntnode[2]){
            {SYNTAX_VAR, NULL},
            {SYNTAX_END, NULL}},
        },
        {SYNTAX_WORD,  "]"},
        {SYNTAX_END,  NULL}}, cl, 0);

    printf("\n");

    // ----
    cl = create_codeline(2, "a", "b");

    match_syntax((syntnode[4]){
        {SYNTAX_WORD,  "["},
            {SYNTAX_LOOP, (syntnode[3]){
                {SYNTAX_VAR,  NULL},
                {SYNTAX_WORD, ","},
                {SYNTAX_END,  NULL}
            }},
        {SYNTAX_WORD,  "]"},
        {SYNTAX_END,  NULL}}, cl, 0);

    printf("\n\n");

    return 0;
}
