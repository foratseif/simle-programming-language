#include <stdlib.h>
#include <stdio.h>

#include "../../src/engine/res/parser.h"
#include "cl.c"

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

enum syntnode_type {
    SYNTAX_WORD,
    SYNTAX_VAR,
    SYNTAX_OBJ
};

typedef struct {
    enum syntnode_type type;
    void*              ptr;
} syntnode;

void print_indent(int x){
    int i;
    for(i=0; i<x; i++){
        printf(" ");
    }
}



int main(void){

    syntnode string_syntax[] = {
        {SYNTAX_WORD, "\""},
        {SYNTAX_VAR,  NULL},
        {SYNTAX_WORD, "\""},
        {SYNTAX_END,  NULL}
    };

    syntnode class[] = {
            {SYNTAX_WORD,  "class"},
            {SYNTAX_VAR,   NULL},
            {SYNTAX_GHOST, (syntnode[]){
                {SYNTAX_WORD, "<"},
                {SYNTAX_VAR,  "="},
                {SYNTAX_END, NULL}
            }},
            {SYNTAX_GHOST, (syntnode[]){
                {SYNTAX_WORD,   "_clib"},
                {SYNTAX_WORD,   "="},
                {SYNTAX_NESTED, string_syntax},
                {SYNTAX_END,    NULL}
            }},
            {SYNTAX_END,  NULL}
    };

    match_syntax(class, create_codeline(2, "class", "shit"), 0);
    printf("\n");

    //match_syntax(class, create_codeline(4, "class", "shit", "<", "fack"), 0);
    //printf("\n");
    //match_syntax(class, create_codeline(7, "class", "shit", "_clib", "=", "\"", "faaaaaaaack", "\""), 0);
    //printf("\n");
    //match_syntax(class, create_codeline(9, "class", "shit", "<", "fack", "_clib", "=", "\"", "faaaaaaaack", "\""), 0);
    //printf("\n");

    return 0;
}
